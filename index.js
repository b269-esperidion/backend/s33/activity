
/*RETRIVED*/
fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => console.log(data))

/*MAP*/
fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => {
    const titles = data.map(item => item.title);
    console.log(titles);
  })

/*RETRIVE SINGLE TO DO LIST ITEM*/
  fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(data => console.log(data))
 

/*PRINTING TITLE AND STATUS*/
 fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(data => {
    const { title, completed } = data;
    console.log(`Title: ${title}, Status: ${completed ? 'Complete' : 'Incomplete'}`);
  });


/*FETCH REQUEST USING POST METHOD*/
fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "New Post",
    completed: false,
    userId: 1
  })

})

.then((response) => response.json())
.then((json) => console.log(json));

/*FETCH REQUEST USING PUT METHOD*/
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "Updated post",
    description: "Update",
    status: "Incomplete",
    dateCompleted : "March 3, 2023",
    userId: 2

    
  })
})
.then((response) => response.json())
.then((json) => console.log(json));

/*FETCH REQUEST USING PATCH METHOD*/
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    status: "Complete",
    dateCompleted : "March 3, 2024"
  
    
  })
})
.then((response) => response.json())
.then((json) => console.log(json));

/*FETCH REQUEST USING DELETE METHOD*/
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE"
});